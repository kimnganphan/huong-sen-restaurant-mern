const mongoose = require("mongoose");

const Product = require("../model/ProductModel");

//CREATE PRODUCT
const createProduct = (req, res) => {
  const newProduct = new Product(req.body);

  newProduct
    .save()
    .then((newProduct) => {
      return res.status(200).json({
        message: "Success",
        product: newProduct,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Fail",
        error: err.message,
      });
    });
};

//UPDATE PRODUCT
const updateProduct = (req, res) => {
  Product.findByIdAndUpdate(
    req.params.productId,
    { $set: req.body },
    { new: true }
  )
    .then((updatedProduct) => {
      return res.status(200).json({
        message: "Success",
        updated: updatedProduct,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Fail",
        error: err.message,
      });
    });
};

//DELETE PRODUCT
const deleteProduct = (req, res) => {
  Product.findByIdAndDelete(req.params.productId)
    .then(() => {
      return res.status(200).json("Product Deleted Successful");
    })
    .catch((err) => {
      res.status(200).json(err);
    });
};

//GET ALL PRODUCT
const getAllProduct = (req, res) => {
  const { page, limit } = req.query;
  Product.find({
    $and: [
      req.query.title
        ? {
            title: { $regex: req.query.title },
          }
        : {},
    ],
  })
    .limit(limit)
    .skip((page - 1) * limit)
    .exec()
    .then((productList) => {
      return res.status(200).json({
        message: "Success",
        products: productList,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Fail",
        error: err.message,
      });
    });
};

//GET PRODUCT BY ID
const getProductById = (req, res) => {
  Product.findOne({ _id: req.params.productId })
    .then((product) => {
      return res.status(200).json({
        message: "Success",
        product: product,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Fail",
        error: err.message,
      });
    });
};
module.exports = {
  createProduct,
  updateProduct,
  deleteProduct,
  getAllProduct,
  getProductById,
};
