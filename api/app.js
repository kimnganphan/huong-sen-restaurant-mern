const express = require("express");
const app = express();
const port = 5000;

const mongoose = require("mongoose");
const dotenv = require("dotenv");

const cors = require("cors");
const productRouter = require('./app/router/ProductRouter')
dotenv.config();

app.use(cors());

app.use(express.json());

app.use(express.urlencoded({
  extended:true
}))
mongoose
  .connect(process.env.MONGO_URL)
  .then(() => console.log("DB Connection Successful"))
  .catch((err) => {
    console.log(err);
  });

app.get("/api/test", (req, res) => {
  res.json({
    message: "Huong Sen Quan Api Ready"
  })
});

app.use("/api/products", productRouter);


app.listen(port, () => {
  console.log("Backend server is running!");
});
