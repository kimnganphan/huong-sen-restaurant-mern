import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home/Home";
import SignIn from "./pages/SignIn/SignIn"
import SignUp from "./pages/SignUp/SignUp"
import About from "./pages/About/About"
import Contact from "./pages/Contact/Contact"
function App() {
  return (
    <Routes>
      <Route exact path="/" element={<Home />}></Route>
      <Route  path="/signin" element={<SignIn />}></Route>
      <Route  path="/signup" element={<SignUp />}></Route>
      <Route  path="/about" element={<About />}></Route>
      <Route  path="/contact" element={<Contact />}></Route>
    </Routes>
  );
}

export default App;
