import { Fragment } from "react";
import Gallery from "../../components/Gallery/Gallery";
import HomeHeader from "../../components/Header/Home/HomeHeader";
import MainNav from "../../components/Navbar/MainNav/MainNav";
import Footer from "../../components/Footer/Footer";
import "./Home.css";
import ProductList from "../../components/ProductList/ProductList";
import Title from "../../components/Title/Title";
import {AiOutlineStar} from 'react-icons/ai'
import { Link } from "react-router-dom";
const Home = () => {
  return (
    <Fragment>
      <MainNav />
      <HomeHeader />
      <div className="productListContainer">
        <div className="container">
          <Title mainTitle="THỰC ĐƠN CÁC MÓN" subTitle="Hương Sen Quán, Tinh Hoa Ẩm Thực Việt" icon={<AiOutlineStar/>} />
          <ProductList />
          <div className="productList-button">
            <button className="btn-white">Xem Tất Cả</button>
          </div>
        </div>
      </div>

      <Gallery />
      <Footer />
    </Fragment>
  );
};

export default Home;
