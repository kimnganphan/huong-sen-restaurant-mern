export const galleryImg = [
    {
        id:1,
        img: "gal-1.jpg"
    },
    {
        id:2,
        img: "gal-2.jpg"
    },
    {
        id:3,
        img: "gal-3.jpg"
    },
    {
        id:4,
        img: "gal-4.jpg"
    },
    {
        id:5,
        img: "gal-10.jpg"
    },
    {
        id:6,
        img: "gal-6.jpg"
    },
    {
        id:7,
        img: "gal-7.jpg"
    },
    {
        id:8,
        img: "gal-8.jpg"
    }
]

export const Products = [
    {
        id:1,
        img: "pho.jpg",
        title: "Phở Bò",
        desc: "Phở là một món ăn truyền thống của Việt Nam, có nguồn gốc từ Hà Nội và Nam Định, và được xem là một trong những món ăn tiêu biểu cho nền ẩm thực Việt Nam.",
        price: 50000,
        cat: ["north", "noddles"]
    },
    {
        id:2,
        img: "banh-xeo.jpg",
        title: "Bánh Xèo",
        desc: "Tại Huế, món ăn này thường được gọi là bánh khoái và thường kèm với thịt nướng, nước chấm là nước lèo gồm tương, gan, lạc. Tại miền Nam Việt Nam, bánh có cho thêm trứng và người ta ăn bánh xèo chấm nước mắm chua ngọt. Tại miền Bắc Việt Nam, nhân bánh xèo ngoài các thành phần như các nơi khác còn thêm củ đậu thái mỏng hoặc khoai môn thái sợi.",
        price: 40000,
        cat: ["middle", "dry"]
    },
    {
        id:3,
        img: "thi-kho-hot-vit.jpg",
        title: "Thịt Kho Hột Vịt",
        desc: "Thịt kho hột vịt (còn gọi là thịt kho tàu hay thịt kho nước dừa) là một món ăn phổ biến tại miền Nam Việt Nam. Món ăn này đặc biệt thường được chế biến để dùng trong các ngày Tết Nguyên Đán vì có thể làm sẵn, giữ được lâu ngày, nên tiện khi dùng bữa thì dọn ra hâm nóng ăn ngay với cơm không phải bận công nấu nướng trong khi vui Tết.",
        price: 70000,
        cat: ["south", "dry"]
    },
    {
        id:4,
        img: "sup-cua.jpg",
        title: "Súp Cua",
        desc: "Súp cua được du nhập từ phương Tây, lúc đầu này chỉ phục vụ tại các nhà hàng lớn, hương và vị cũng khác như hiện tại. Những người đầu tiên ăn món này đã tìm tòi cách chế biến sau đó gia giảm để phù hợp với người bản địa. Dễ ăn, dễ nấu, dần dần súp cua trở thành món ăn bình dân phổ biến. Súp cua dậy mùi thơm thịt cua tươi, sánh màu ngà ngon lành là món ăn bổ sung nhiều iod, calci và các chất khoáng cung cấp cho quá trình tái tạo và phục hồi sinh lực của cơ thể.",
        price: 30000,
        cat: ["south", "noddles"]
    },
    {
        id:5,
        img: "bo-kho.jpg",
        title: "Bò Kho",
        desc: "Bò kho là một món ăn chế biến từ thịt bò với phương pháp kho, có xuất xứ ở miền Nam Việt Nam. Món này được nhiều người ưa thích. Nguyên bản món Bò kho được người miền Nam Việt Nam dùng kèm với nhiều loại rau mùi, để tăng hương vị món ăn. Tuy có tên gọi là bò kho nhưng cách thức chế biến chủ đạo của món ăn lại là hầm, hình thức kho được dùng để tẩm ướp và làm săn chắc thịt bò trước khi hầm. Món này cũng nổi tiếng bên ngoài Việt Nam",
        price: 60000,
        cat: ["south", "noddles"]
    },
    {
        id:6,
        img: "banh-khot.jpg",
        title: "Bánh Khọt Kho",
        desc: "Bánh khọt là loại bánh Việt Nam (chính xác là loại bánh đặc trưng của miền nam Việt Nam) làm từ bột gạo hoặc bột sắn, có nhân tôm, được nướng và ăn kèm với rau sống, ớt tươi, thường ăn với nước mắm pha ngọt, rất ít khi chấm nước sốt mắm tôm (không phải mắm tôm hay mắm tôm chua).",
        price: 60000,
        cat: ["south", "dry"]
    },
    
]