import "./Gallery.css";
import { galleryImg } from "../../data";
import { BsFillCameraFill } from "react-icons/bs";
import Title from "../../components/Title/Title";
const Gallery = () => {
  return (
    <div className="container">
      <div className="gallery__wrapper">
        {/* ----------- TITLE ---------- */}
        <Title
          mainTitle="THƯ VIỆN ẢNH"
          subTitle="Những khoảnh khắc tuyệt đẹp của món ăn được ghi lại"
          icon={<BsFillCameraFill />}
        />

        {/* ----------- END OF TITLE ---------- */}

        {/* ----------- GALLERY IMAGE CARD ---------- */}
        <div className="galleryImageCard">
          <div className="galleryImageCard__wrapper">
            {galleryImg.map((item) => {
              return (
                <img
                  src={require(`../../assets/image/gallery/${item.img}`)}
                  alt=""
                  key={item.id}
                />
              );
            })}
          </div>
        </div>
        {/* -----------END OF GALLERY IMAGE CARD ---------- */}
      </div>
    </div>
  );
};

export default Gallery;
