import "./ProductList.css";
import { Products } from "../../data";
import { numberWithCommas } from "../../function";
const ProductList = () => {
  return (
    <div className="productList__cardsContainer">
      {Products.map((item) => {
        return (
          <div className="productList__card" key={item.id}>
            <div className="productList__cardImg">
              <div className="productList__cardImg-image">
                <img
                  src={require(`../../assets/image/food/${item.img}`)}
                  alt=""
                />
              </div>
              <div className="productList__cardImg-star"></div>
            </div>
            <div className="productList__cardTitle">{item.title}</div>
            <div className="productList__price">
              {numberWithCommas(item.price)} VND
            </div>

            <div className="productList__button">
              <button className="btn-green">Xem Thêm</button>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default ProductList;
