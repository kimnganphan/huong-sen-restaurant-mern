import "./Title.css";
const Title = (props) => {
  return (
    <div className="titleContainer">
      <p className="titleContainer-icon">
        {props.icon}
      </p>
      <h2>{props.mainTitle}</h2>
      <p>{props.subTitle}</p>
    </div>
  );
};

export default Title;
