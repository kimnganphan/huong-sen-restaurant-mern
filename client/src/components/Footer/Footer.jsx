import "./Footer.css";
import { AiOutlineArrowUp } from "react-icons/ai";
import { Link } from "react-router-dom";
import { GiLotus } from "react-icons/gi";
import { AiOutlineInstagram } from "react-icons/ai";
import { FaFacebook,FaYoutube } from "react-icons/fa";
const Footer = () => {
  return (
    <footer>
      <div className="footer__wrapper">
        {/* --------- BACK TO TOP BUTTON ----------- */}
        <div className="footer__backToTop">
          <button className="btn-brown">
            <AiOutlineArrowUp />
          </button>
        </div>
        {/* --------- END OF BACK TO TOP BUTTON ----------- */}

        {/* --------- MAIN CONTENT ----------- */}
        <div className="footer__mainContent">
          <div className="footer__mainContent-left">
            <div className="footerLeft__pages">
              <h4>Các Trang</h4>
              <Link to="/">Trang Chủ</Link>
              <Link to="/about">Về Chúng Tôi</Link>
              <Link to="/contact">Liên Hệ</Link>
              <Link to="/signup">Đăng Kí</Link>
              <Link to="/signin">Đăng Nhập</Link>
              <Link to="/cart">Giỏ Hàng</Link>
            </div>

            <div className="footerLeft-menu">
              <h4>Thực Đơn</h4>
              <a href="#">Đặc Sản Miền Bắc</a>
              <a href="#">Đặc Sản Miền Trung</a>
              <a href="#">Đặc Sản Miền Nam</a>
              <a href="#">Món Khô</a>
              <a href="#">Món Nước</a>
              <a href="#">Món Chay</a>
              <a href="#">Đồ Uống</a>
            </div>
          </div>
          <div className="footer__mainContent-right">
            <div className="footerRight-logo">
              Hương Sen <br /> Quán <GiLotus className="footerRight-logo-icon" />
            </div>
            <div className="footerRight-socials">
              <h5>Follow Us</h5>
              <div>
              <a href="#"><AiOutlineInstagram/></a>
              <a href="#"><FaFacebook/></a>
              <a href="#"><FaYoutube/></a>
              </div>
              
            </div>
          </div>
        </div>

        {/* --------- END OF MAIN CONTENT ----------- */}

        {/* --------- COPYRIGHT ----------- */}
        <div className="footer__copyright">&copy; Huong Sen Quan 2022</div>
        {/* --------- END OF COPYRIGHT ----------- */}
      </div>
    </footer>
  );
};

export default Footer;
