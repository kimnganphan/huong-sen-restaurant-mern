import "./MainNav.css";
import { Link } from "react-router-dom";
import { AiOutlineShoppingCart, AiOutlineUser } from "react-icons/ai";
import {GiLotus} from 'react-icons/gi'
const MainNav = () => {
  return (
    <nav className="mainNav">
      <div className="nav__wrapper">
        {/* ----------- NAVBAR LEFT --------- */}
        <div className="nav__wrapper-left">
          <div className="navLeft-navigation">
            <Link to="/">Trang Chủ</Link>
            <Link to="/">Thực Đơn</Link>
            <Link to="/about">Về Chúng Tôi</Link>
            <Link to="/contact">Liên Hệ</Link>
          </div>
        </div>
        {/* ----------- END OF NAVBAR LEFT --------- */}

        {/* ----------- NAVBAR CENTER --------- */}
        <div className="nav__wrapper-center">
          <div className="navCenter-logo">
            Hương Sen <br /> Quán <GiLotus className="navCenter-logo-icon"/>
          </div>
        </div>

        {/* ----------- END OF NAVBAR CENTER --------- */}

        {/* ----------- NAVBAR RIGHT --------- */}
        <div className="nav__wrapper-right">
          <Link to="/signin">
            <AiOutlineUser /> Đăng Nhập
          </Link>

          <Link to="signin">
            <AiOutlineShoppingCart /> Giỏ Hàng
          </Link>
        </div>
        {/* ----------- END OF NAVBAR RIGHT --------- */}
      </div>
    </nav>
  );
};

export default MainNav;
