import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  title: "",
  price: "",
  desc: "",
  product: {},
  products: [],
  paginationProducts: []
};

export const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {
    defaultTitle: (state) =>{
      state.title = state.product.title
    },
    setTitle: (state, action) => {
      
      state.title = action.payload;
    },
    defaultDesc: (state) =>{
      state.desc = state.product.desc
    },
    setDesc: (state, action) => {
      state.desc = action.payload;
    },
    defaultPrice: (state) =>{
      state.price = state.product.price
    },
    setPrice: (state, action) => {
      state.price = action.payload;
    },
    
    createProduct: (state, action) => {
      state.products.push(action.payload);
    },
    getAllProduct: (state, action) => {
      state.products = action.payload;
    },
    getOneProduct: (state, action) => {
      state.product = action.payload;
    },
    updateProduct: (state, action) => {
      state.products[
        state.products.findIndex((item) => item._id === action.payload.id)
      ] = action.payload.product;
    },
    deleteProduct: (state, action) => {
      state.products.splice(
        state.products.findIndex((item) => item._id === action.payload),
        1
      );
    },
    getPaginationProducts: (state, action) => {
      state.paginationProducts = action.payload;
    }
  },
});

export const {
  defaultTitle,
  setTitle,
  defaultDesc,
  setDesc,
  defaultPrice,
  setPrice,
  createProduct,
  getAllProduct,
  deleteProduct,
  getOneProduct,
  updateProduct,
  getPaginationProducts
} = productSlice.actions;

export default productSlice.reducer;
