import { Link } from 'react-router-dom'
import './Navbar.css'

const Navbar = () => {
  return (
    <nav>
      <div className="nav__wrapper">
        {/* --------- NAVBAR LEFT --------- */}
        <div className="nav__wrapperLeft">
          <div className="nav__wrapperLeft-title">Theme:</div>
          <div className='nav__wrapperLeft-theme'>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
        {/* ---------END OF NAVBAR LEFT --------- */}

        {/* --------- NAVBAR RIGHT --------- */}
        <div className="nav__wrapperRight">
          <Link to="/login">
            Đăng Nhập
          </Link>
        </div>
        {/* ---------END OF NAVBAR RIGHT --------- */}
      </div>
    </nav>
  )
}

export default Navbar