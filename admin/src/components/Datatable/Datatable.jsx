import "./Datatable.css";
import { useDispatch, useSelector } from "react-redux";
import { deleteProductByProductId } from "../../callApi";
import { Link, useLocation } from "react-router-dom";

import { Modal } from "../Modal";
import { useState } from "react";
import Buttons from "../Details/ProductDetails/Buttons/Buttons";
const Datatable = ({ tableTitle, dataLoad }) => {
  const location = useLocation();

  const [isVisible, setIsVisible] = useState(false);
  const [productId, setProductId] = useState()
  //Hàm xử lý ấn nút delete
  const handleDelete = (e, id) => {
    // deleteProductByProductId(id, dispatch);
    setIsVisible(true);
    setProductId(id)
  };
  return (
    <div className="table__wrapper">
      {/* -------- TABLE --------- */}
      <table>
        <thead>
          <tr>
            <th>STT</th>
            <th>{tableTitle} </th>
            <th>Hoạt động</th>
          </tr>
        </thead>
        <tbody>
          {dataLoad.map((item, index) => {
            return (
              <tr key={index}>
                <td>{index + 1}</td>
                <td className="table__mainContent">
                  {item.img && (
                    <img src={item.img} alt="" className="tableRow__image" />
                  )}{" "}
                  <span>{item.title || item.name || item.orderId} </span>
                </td>
                <td className="table__button">
                  <Link to={`${location.pathname}/${item._id}`}>
                    <button className="table__button-edit">Edit</button>
                  </Link>

                  <button
                    className="table__button-delete"
                    onClick={(e) => handleDelete(e, item._id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      {/* -------- END OF TABLE --------- */}

      {/* -------- MODAL --------- */}
      <Modal
        isVisible={isVisible}
        onClose={() => setIsVisible(false)}
        title="Alert Modal"
        content="Bạn có chắc muốn xóa dữ liệu này?"
        footer={<Buttons type="delete" setIsVisible={setIsVisible} productId={productId}/>}
      />
      {/* -------- END OF MODAL --------- */}
    </div>
  );
};

export default Datatable;
