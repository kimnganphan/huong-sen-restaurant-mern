import "./Buttons.css";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import {
  updateProductByProductId,
  deleteProductByProductId,
  getProductByProductId
} from "../../../../callApi";
import { useEffect } from "react";

const Buttons = ({ setIsVisible, type, productId }) => {
  const id = useParams();
  const state = useSelector((state) => state.product);
  const dispatch = useDispatch();

  //HÀM XỬ LÝ SỰ KIỆN ẤN NÚT CANCEL
  const handleCancelClick = () => {
    setIsVisible(false);
  };

  //HÀM XỬ LÝ SỰ KIỆN ẤN NÚT CONFIRM
  const handleConfirmClick = () => { 
    if (type === "edit") {// NẾU TYPE CỦA MODAL LÀ EDIT
      const finalProduct = {
        ...state.product,
        title: state.title,
        price: state.price,
        desc: state.desc,
      };

      updateProductByProductId(
        id.productId,
        finalProduct,
        dispatch,
        setIsVisible
      );
      
    } else if (type === "delete") { // NẾU TYPE CỦA MODAL LÀ DELETE
      deleteProductByProductId(productId, dispatch, setIsVisible);
    }
  };

  
  return (
    <div className="buttons">
      {/* ---- CANCEL BUTTON ------- */}
      <button className="btn-cancel" onClick={handleCancelClick}>
        Cancel
      </button>
      {/* ---- END OF CANCEL BUTTON ------- */}

      {/* -------- CONFIRM BUTTON ------ */}
      <button className="btn-confirm" onClick={handleConfirmClick}>
        Confirm
      </button>
      {/* -------- END OF CONFIRM BUTTON ------ */}
    </div>
  );
};

export default Buttons;
