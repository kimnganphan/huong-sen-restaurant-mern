import React, { useState } from "react";
import "./ProductDetails.css";
import { useSelector, useDispatch } from "react-redux";
import { numberWithCommas } from "../../../function";
import { Modal } from "../../Modal";

import Buttons from "./Buttons/Buttons";
import EditInput from "./EditInput/EditInput";
import {
  defaultTitle,
  defaultDesc,
  defaultPrice,
} from "../../../redux/Product";
const ProductDetails = () => {
  //LẤY THÔNG TIN TỪ PRODUCT REDUX
  const product = useSelector((state) => state.product.product);
  const dispatch = useDispatch();
  //CÁC BIẾN CHỨA THÔNG TIN CỦA MODAL
  const [isVisible, setIsVisible] = useState(false);
  const [modalLarge, setModalLarge] = useState("");

  const [footer, setFooter] = useState("");
  const [content, setContent] = useState("");

  //HÀM RENDER LẠI PHÂN LOẠI SẢN PHẨM
  const changeCat = (cat) => {
    if (cat == "middle") return "Đặc Sản Miền Trung";
    if (cat == "north") return "Đặc Sản Miền Bắc";
    if (cat == "south") return "Đặc Sản Miền Nam";
    if (cat == "dry") return "Đồ Khô";
    if (cat == "noddle") return "Đồ Nước";
    if (cat == "vegan") return "Đồ Chay";
  };

  const handleEditClick = () => {
    //THÊM CÁC COMPONENT VÀO MODAL ĐỂ HIỂN THỊ THÔNG TIN
    setContent(<EditInput />);
    setFooter(<Buttons setIsVisible={setIsVisible} type="edit"/>);
    setIsVisible(true);
    setModalLarge("modal-lg");

    // DISPATCH CÁC HÀM ĐỂ ĐỊNH DẠNG DEFAULT VALUE CHO INPUT
    dispatch(defaultTitle(product.title));
    dispatch(defaultDesc(product.desc));
    dispatch(defaultPrice(product.price));
  };

  return (
    <div className="productDetail">
      {/* ----- MODAL HIỂN THỊ CHI TIẾT SẢN PHẨM ----- */}
      <Modal
        isVisible={isVisible}
        title="CHI TIẾT SẢN PHẨM"
        onClose={() => setIsVisible(false)}
        modalLarge={modalLarge}
        content={content}
        footer={footer}
      />
       {/* ----- END OF MODAL HIỂN THỊ CHI TIẾT SẢN PHẨM ----- */}
      {/* ------------ TITLE ----------- */}
      <div className="productDetail__title">
        <h2>CHI TIẾT SẢN PHẨM</h2>
      </div>
      {/* ------------ END OF TITLE ----------- */}

      {/* ------------ PRODUCT DETAILS ----------- */}
      <div className="productDetail__details">
        {/* ---- PRODUCT THUMBNAIL IMAGE ----- */}
        <div className="productDetail__details-item">
          <label>Ảnh đại diện: </label>
          <div className="productDetail__details-item-content">
            <img src={product.img} alt="" />
          </div>
        </div>
        {/* ---- END OF PRODUCT THUMBNAIL IMAGE ----- */}

        {/* ---- PRODUCT NAME ----- */}
        <div className="productDetail__details-item">
          <label>Tên Sản Phẩm:</label>
          <div className="productDetail__details-item-content">
            <p className="contentTitle">{product.title?.toUpperCase()}</p>
          </div>
        </div>
        {/* ---- END OF PRODUCT NAME ----- */}

        {/* ---- PRODUCT DESCRIPTION ----- */}
        <div className="productDetail__details-item">
          <label>Miêu Tả Sản Phẩm:</label>
          <div className="productDetail__details-item-content">
            <p>{product.desc}</p>
          </div>
        </div>
        {/* ----END OF PRODUCT DESCRIPTION ----- */}

        {/* ---- PRODUCT CATEGORIES ----- */}
        <div className="productDetail__details-item">
          <label>Phân Loại Sản Phẩm:</label>
          <div className="productDetail__details-item-content">
            {product.categories?.map((item, index) => {
              return <p className="contentCat">{changeCat(item)}</p>;
            })}
          </div>
        </div>
        {/* ----END OF PRODUCT CATEGORIES ----- */}

        {/* ---- PRODUCT PRICE ----- */}
        <div className="productDetail__details-item">
          <label>Gía Sản Phẩm:</label>
          <div className="productDetail__details-item-content">
            <p>{product.price && numberWithCommas(product.price)} VNĐ</p>
          </div>
        </div>
        {/* ----END OF PRODUCT PRICE ----- */}

        {/* ---- PRODUCT GALLERY IMAGE ----- */}
        <div className="productDetail__details-item">
          <label>Thư Viện Ảnh: </label>
          <div className="productDetail__details-item-content">
            {product.galleryImg?.map((item, index) => {
              return <img src={item} alt={item} key={index} />;
            })}
          </div>
        </div>
        {/* ----END OF PRODUCT GALLERY IMAGE ----- */}
      </div>
      {/* ------------ END OF PRODUCT DETAILS ----------- */}

      {/* ------------ EDIT BUTTON ----------- */}
      <div className="productDetail__button">
        <button className="btn-light" onClick={handleEditClick}>
          Edit Button
        </button>
      </div>
      {/* ------------ END OF EDIT BUTTON ----------- */}
    </div>
  );
};

export default ProductDetails;
