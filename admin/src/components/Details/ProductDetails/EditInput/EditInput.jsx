import { useDispatch, useSelector } from "react-redux";
import { setTitle, setPrice, setDesc} from "../../../../redux/Product";
import "./EditInput.css";

const EditInput = () => {
  
  const state = useSelector((state) => state.product);
  const dispatch = useDispatch();
  //HÀM XỬ LÝ LẤY GIÁ TRỊ TITLE TÊN SẢN PHẨM
  const handleTitle = (e) => {
    dispatch(setTitle(e.target.value));
  };
  //HÀM XỬ LÝ LẤY GIÁ TRỊ DESC MIÊU TẢ SẢN PHẨM
  const handleDesc = (e) => {
    dispatch(setDesc(e.target.value));
  };

  //HÀM XỬ LÝ LẤY GIÁ TRỊ PRICE CỦA SẢN PHẨM
  const handlePrice = (e) => {
    dispatch(setPrice(Number(e.target.value)));
  };
 
  return (
    <div className="editInput__wrapper">
      {/* ---- PRODUCT NAME ----- */}
      <div className="editInput-item">
        <label>Tên Sản Phẩm:</label>
        <div className="editInput-item-content">
          <input type="text" value={state.title} onChange={(e) => handleTitle(e)} />
        </div>
      </div>
      {/* ---- END OF PRODUCT NAME ----- */}

      {/* ---- PRODUCT DESC ----- */}
      <div className="editInput-item">
        <label>Miêu Tả Sản Phẩm:</label>
        <div className="editInput-item-content">
          <textarea
            cols="30"
            rows="10"
            value={state.desc}
            onChange={(e) => handleDesc(e)}
          ></textarea>
        </div>
      </div>
      {/* ---- END OF PRODUCT DESC ----- */}

      {/* ---- PRODUCT NAME ----- */}
      <div className="editInput-item">
        <label>Gía Sản Phẩm:</label>
        <div className="editInput-item-content">
          <input type="text" value={state.price} onChange={(e) => handlePrice(e)} />
        </div>
      </div>
      {/* ---- END OF PRODUCT NAME ----- */}
    </div>
  );
};

export default EditInput;
