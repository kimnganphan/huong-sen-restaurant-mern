import "./Pagination.css";
import icons from "../../assets/icons";
import { useSelector } from "react-redux";

const Pagination = ({ setPage, page, limit }) => {
  //GET ALL PRODUCTS STATE FROM REDUX
  const productList = useSelector((state) => state.product.products);
  //HANDLE ONCLICK CHANGE PAGE EVENT
  const handleClickPagination = (e, type, index) => {
    switch (type) {
      case "first-page":
        setPage(1);
        break;
      case "previous-page":
        setPage(page - 1);
        break;
      case "every-page":
        setPage(index);
        console.log(index);
        break;
      case "next-page":
        setPage(page + 1);
        break;
      case "last-page":
        setPage(Math.ceil(productList.length / limit));
        break;
      default:
        break;
    }
  };
  return (
    <div className="pagination">
      <div className="pagination__wrapper">
        {/* ---- GO TO FIRST PAGE ------ */}
        <div
          className="pagination-item"
          onClick={(e) => handleClickPagination(e, "first-page")}
        >
          <a href="#">{icons.doubleArrLeft}</a>
        </div>
        {/* ----END OF GO TO FIRST PAGE ------ */}

        {/* ---- GO TO PREVIOUS PAGE ------ */}
        <div
          className="pagination-item"
          onClick={(e) => handleClickPagination(e, "previous-page")}
        >
          <a href="#">{icons.arrLeft}</a>
        </div>
        {/* ---- END OF GO TO PREVIOUS PAGE ------ */}

        {/* ---- GO TO PAGE ------ */}
        <div className="pagination-item">
          {[...Array(productList.length)].map((product, index) => {
            if (index >= 1 && index <= Math.ceil(productList.length / limit)) {
              return (
                <a
                  href="#"
                  key={index}
                  className={page === index ? "activePagi" : ""}
                  onClick={(e) => handleClickPagination(e, "every-page", index)}
                >
                  {index}
                </a>
              );
            }
          })}
        </div>
        {/* ---- END OF GO TO PAGE ------ */}

        {/* ---- GO TO NEXT PAGE ------ */}
        <div
          className="pagination-item"
          onClick={(e) => handleClickPagination(e, "next-page")}
        >
          <a href="#">{icons.arrRight}</a>
        </div>
        {/* ----END OF GO TO NEXT PAGE ------ */}

        {/* ---- GO TO LAST PAGE ------ */}
        <div
          className="pagination-item"
          onClick={(e) => handleClickPagination(e, "last-page")}
        >
          <a href="#">{icons.doubleArrRight}</a>
        </div>
        {/* ----END OF GO TO LAST PAGE ------ */}
      </div>
    </div>
  );
};

export default Pagination;
