import "./Sidebar.css";
import { Link, useLocation, NavLink } from "react-router-dom";
import icons from "../../assets/icons";
import { useEffect, useState } from "react";
const Sidebar = () => {
  const { dashIcon, storeIcon, userIcon, billIcon, viewIcon } = icons;

  return (
    <div className="sidebar">
      <div className="sidebar__wrapper">
        {/* ----------- SIDEBAR LOGO ---------- */}
        <div className="sidebar__wrapper-logo">Admin</div>
        {/* ----------- END OF SIDEBAR LOGO ---------- */}

        {/* ------------DASHBOARD--------------------- */}
        <div className="sidebar__wrapper-dashboard">
          {/* --- store---- */}
          <div className="title"> {dashIcon} Danh mục</div>
          <div className="items">
            <NavLink
              to="/"
              className={({ isActive }) =>
                isActive ? "activeClassName" : undefined
              }
            >
              {viewIcon} Trang Chủ
            </NavLink>

            <NavLink
              to="/products"
              className={({ isActive }) =>
                isActive ? "activeClassName" : undefined
              }
            >
              {storeIcon} Sản Phẩm
            </NavLink>

            <NavLink
              to="/users"
              className={({ isActive }) =>
                isActive ? "activeClassName" : undefined
              }
            >
              {userIcon} Khách Hàng
            </NavLink>

            <NavLink
              to="/orders"
              className={({ isActive }) =>
                isActive ? "activeClassName" : undefined
              }
            >
              {billIcon} Đơn Hàng
            </NavLink>
          </div>
        </div>
        {/* ------------ END OF DASHBOARD--------------------- */}
      </div>
    </div>
  );
};

export default Sidebar;
