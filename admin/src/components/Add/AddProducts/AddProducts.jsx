import "./AddProducts.css";
import { useEffect, useRef, useState } from "react";
import Modal from "../../Modal/Modal";
import {
  getStorage,
  ref,
  uploadBytesResumable,
  getDownloadURL,
} from "firebase/storage";
import { app } from "../../../firebase";
import { useSelector, useDispatch } from "react-redux";
import { addProduct } from "../../../callApi";
const AddProducts = () => {
  //Phân loại sản phẩm checkbox
  const productType = ["north", "south", "middle", "dry", "noddle", "vegan"];
  const [changeStyle, setChangeStyle] = useState([
    "Đặc Sản miền Bắc",
    "Đặc Sản miền Nam",
    "Đặc Sản miền Trung",
    "Đồ Khô",
    "Đồ Nước",
    "Đồ Chay",
  ]);

  //Set ẩn hiện modal cảnh báo nếu thiếu dữ liệu
  const [isVisible, setIsVisible] = useState(false);
  const [content, setContent] = useState("");
  //Các trường input dữ liệu
  const [file, setFile] = useState(null);
  const [fileArr, setFileArr] = useState([]);
  const [galleryImg, setGalleryImg] = useState([]);
  const [title, setTitle] = useState("");
  const titleRef = useRef();
  const [desc, setDesc] = useState("");
  const [price, setPrice] = useState("");
  const [categories, setCategories] = useState([]);
  const [imgSrc, setImgSrc] = useState([]);
  //Làm việc với redux
  const state = useSelector((state) => state.product);
  const dispatch = useDispatch();

  //Hàm xử lý sự kiện checkbox
  const handleCheckType = (type) => {
    setCategories((prev) => {
      const isChecked = categories.includes(type);
      if (isChecked) {
        return categories.filter((item) => item !== type);
      } else {
        return [...prev, type];
      }
    });
  };
  //hàm xử lý sự kiện up ảnh thumbnail
  const handleChangeThumbnailImg = (e) => {
    setFile(e.target.files[0]);

    e.target.value = null;
  };
  //Xử lý sự kiện up ảnh thumbnail
  useEffect(() => {
    if (file !== null) {
      const storage = getStorage(app);
      const storageRef = ref(storage, file.name);

      const uploadTask = uploadBytesResumable(storageRef, file);

      uploadTask.on(
        "state_changed",
        (snapshot) => {
          // Observe state change events such as progress, pause, and resume
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          const progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log("Upload is " + progress + "% done");
          switch (snapshot.state) {
            case "paused":
              console.log("Upload is paused");
              break;
            case "running":
              console.log("Upload is running");
              break;
            default:
          }
        },
        (error) => {
          // Handle unsuccessful uploads
          setIsVisible(true);
          setContent(error);
        },
        () => {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            setImgSrc(downloadURL);
          });
        }
      );
    }
  }, [file]);

  //Hàm xử lý sự kiện thêm mới sản phẩm
  const handleAddNewProduct = (e) => {
    e.preventDefault();

    var isValid = validateData();

    if (isValid) {
      addProduct(
        { title, desc, img: imgSrc, price, categories, galleryImg },
        dispatch,
        resetData
      );
    }
  };

  //Hàm validate thông tin sản phẩm
  const validateData = () => {
    if (file === null) {
      setContent("Thiếu Ảnh Đại Diện Sản Phẩm");

      setIsVisible(true);
      return false;
    }
    if (title.trim() === "") {
      setContent("Thiếu Tên Sản Phẩm");

      setIsVisible(true);
      return false;
    }
    if (desc.trim() === "") {
      setContent("Thiếu Miêu Tả Sản Phẩm");
      setIsVisible(true);
      return false;
    }
    if (price.trim() === "") {
      setContent("Thiếu Gía Sản Phẩm");
      setIsVisible(true);
      return false;
    }

    if (isNaN(price)) {
      setContent("Gía Sản Phẩm Không Hợp Lệ");
      setIsVisible(true);
      return false;
    }

    if (categories.length === 0) {
      setContent("Vui lòng chọn phân loại sản phẩm");
      setIsVisible(true);
      return false;
    }
    return true;
  };
  //Hàm xử lý sự kiện onchange của thư viện hình ảnh sản phẩm
  const handleChangeImageGallery = (e) => {
    setFileArr([...fileArr, e.target.files[0]]);

    e.target.value = "";
  };
  //xử lý sự kiện load ảnh trong gallery image lên
  useEffect(() => {
    if (fileArr.length !== 0) {
      fileArr.map((item, index) => {
        const storage = getStorage(app);
        const storageRef = ref(storage, item.name);
        const uploadTask = uploadBytesResumable(storageRef, item);

        uploadTask.on(
          "state_changed",
          (snapshot) => {
            // Observe state change events such as progress, pause, and resume
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            const progress =
              (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log("Upload is " + progress + "% done");
            switch (snapshot.state) {
              case "paused":
                console.log("Upload is paused");
                break;
              case "running":
                console.log("Upload is running");
                break;
              default:
            }
          },
          (error) => {
            // Handle unsuccessful uploads
            setIsVisible(true);
            setContent(error);
          },
          () => {
            // Handle successful uploads on complete
            // For instance, get the download URL: https://firebasestorage.googleapis.com/...
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
              setGalleryImg([...galleryImg, downloadURL]);
            });
          }
        );
      });
    }
  }, [fileArr]);

  //Reset lại các trường dữ liệu xong khi gọi api thành công
  const resetData = () => {
    setTitle("");
    setDesc("");
    setPrice("");
    setCategories([]);
    setFile(null);
    setFileArr([]);
    setGalleryImg([]);
    setImgSrc([]);
  };

  return (
    <div className="addProduct">
      {/* ------------- TITLE ------------ */}
      <div className="addProduct-title">
        <h2>Thêm Sản Phẩm Mới</h2>
      </div>
      {/* ------------- END OF TITLE ------------ */}

      {/* ------------- CONTENT ------------ */}
      <div className="addProduct__contents">
        {/* ------------ PRODUCT THUMBNAIL ----------- */}
        <div className="addProduct__contents-item productThumbnailImage">
          <img
            src={
              file === null
                ? "https://media.istockphoto.com/vectors/no-image-available-sign-vector-id922962354?k=20&m=922962354&s=612x612&w=0&h=f-9tPXlFXtz9vg_-WonCXKCdBuPUevOBkp3DQ-i0xqo="
                : imgSrc
            }
            alt="product-thumnail"
          />
          <input type="file" onChange={(e) => handleChangeThumbnailImg(e)} />
        </div>
        {/* ------------END OF PRODUCT THUMBNAIL ----------- */}

        {/* ------------ PRODUCT NAME ----------- */}
        <div className="addProduct__contents-item">
          <label>Tên Sản Phẩm:</label>
          <input
            ref={titleRef}
            value={title}
            type="text"
            placeholder="Nhập tên sản phẩm..."
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        {/* ------------ END OF PRODUCT NAME ----------- */}

        {/* ------------ PRODUCT DESCRIPTION ----------- */}
        <div className="addProduct__contents-item">
          <label>Miêu tả sản phẩm:</label>
          <textarea
            value={desc}
            onChange={(e) => setDesc(e.target.value)}
            placeholder="Nhập miêu tả sản phẩm..."
            cols="30"
            rows="10"
          ></textarea>
        </div>
        {/* ------------END OF PRODUCT DESCRIPTION ----------- */}

        {/* ------------ PRODUCT PRICE ----------- */}
        <div className="addProduct__contents-item">
          <label>Gía Sản Phẩm:</label>
          <input
            value={price}
            type="text"
            placeholder="Nhập gia sản phẩm..."
            onChange={(e) => setPrice(e.target.value)}
          />
        </div>
        {/* ------------ END OF PRODUCT PRICE ----------- */}

        {/* ------------ PRODUCT TYPE ----------- */}
        <div className="addProduct__contents-item">
          <label>Phân Loại Sản Phẩm:</label>
          {productType.map((type, index) => {
            return (
              <div key={type} className="addProduct__contents-item-checkbox">
                <input
                  type="checkbox"
                  checked={categories.includes(type)}
                  onChange={() => handleCheckType(type)}
                  id={type}
                />
                <label htmlFor={type}>{changeStyle[index]}</label>
              </div>
            );
          })}
        </div>
        {/* ------------ END OF PRODUCT TYPE ----------- */}

        {/* --------------- PRODUCT IMAGE GALLERY -------- */}

        {/* --------------- END OF PRODUCT IMAGE GALLERY -------- */}
        <div className="addProduct__contents-item imageGallery">
          <label>Thư viện hình ảnh sản phẩm</label>

          <input type="file" onChange={(e) => handleChangeImageGallery(e)} />

          <div className="imageGalleryDisplay">
            {galleryImg.map((item, index) => {
              return <img src={item} alt="" key={index} />;
            })}
          </div>
        </div>
        {/* ------------ ADD NEW BUTTON ----------- */}
        <div className="addProduct__contents-item addNewButton">
          <button className="btn-light" onClick={handleAddNewProduct}>
            Thêm Mới
          </button>
        </div>
        {/* ------------ END OF ADD NEW BUTTON ----------- */}
      </div>
      {/* ------------- END OF CONTENT ------------ */}
      <Modal
        title="Alert Modal"
        isVisible={isVisible}
        onClose={() => setIsVisible(false)}
        content={content}
      />
    </div>
  );
};

export default AddProducts;
