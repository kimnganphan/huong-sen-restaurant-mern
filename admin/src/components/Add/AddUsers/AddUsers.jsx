import { useEffect, useState } from 'react'
import './AddUsers.css'

const AddUsers = () => {
  const [fileArr, setFileArr] = useState([])
  const handleFile = (e) =>{
    setFileArr([...fileArr,e.target.files[0]])
  }
  

  return (
    <div>
      <div>
        {fileArr.map((item,index)=>{
          <img src={item.name} alt="" />
        })}
      </div>
      <input type="file" onChange={e=>handleFile(e)}/>
    </div>
  )
}

export default AddUsers