import {
  createProduct,
  getAllProduct,
  deleteProduct,
  getOneProduct,
  updateProduct,
  setTitle,
  setDesc,
  setPrice,
  getPaginationProducts
} from "./redux/Product";
import axios from "axios";

//HÀM XỬ LÝ SỰ KIỆN THÊM MỚI SẢN PHẨM
export const addProduct = (product, dispatch, clear) => {
  axios
    .post("http://localhost:5000/api/products", product)
    .then((res) => {
      dispatch(createProduct(res.data.product));
      //xóa dữ liệu trên form sau khi gọi api thành công
      clear();
    })
    .catch((error) => {
      alert(error);
    });
};

//HÀM XỬ LÝ SỰ KIỆN XÓA SẢN PHẨM THEO ID
export const deleteProductByProductId = (productId, dispatch, setIsVisible) => {
  axios
    .delete("http://localhost:5000/api/products/" + productId)
    .then(() => {
      dispatch(deleteProduct(productId));
      setIsVisible(false)
    })
    .catch((error) => {
      alert(error);
    });
};

//HÀM XỬ LÝ SỰ KIỆN LẤY PRODUCT BY ID
export const getProductByProductId = (productId, dispatch) => {
  axios
    .get("http://localhost:5000/api/products/" + productId)
    .then((res) => {
      dispatch(getOneProduct(res.data.product));
    })
    .catch((error) => {
      alert(error);
    });
};

//HÀM XỬ LÝ SỰ KIỆN LẤY TẤT CẢ CÁC SẢN PHẨM
export const getAllProductsFromApi = async (dispatch, title, limit, page) => {
  try {
    const res = await axios.get(
      `http://localhost:5000/api/products?title=${title}&limit=${limit}&page=${page}`
    );
    if(limit === 0){
      dispatch(getAllProduct(res.data.products));
    } else {
      dispatch(getPaginationProducts(res.data.products))
    }
    
  } catch (error) {}
};


//HÀM XỬ LÝ SỰ KIỆN UPDATE PRODUCT BY PRODUCT ID
export const updateProductByProductId = (productId, product, dispatch, setIsVisible) =>{
  axios
    .put("http://localhost:5000/api/products/" + productId, product)
    .then((res) => {
      dispatch(updateProduct({id:productId, product:res.data.updated} ));
      //TẮT MODAL
      setIsVisible(false)
      //RESET LẠI CÁC TRƯỜNG DỮ LIỆU
      dispatch(setTitle(""));
      dispatch(setDesc(""));
      dispatch(setPrice(("")));
    })
    .catch((error) => {
      alert(error);
    });
}