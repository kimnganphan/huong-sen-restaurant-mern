import { Login } from "./pages/Login";
import { Home } from "./pages/Home";
import { List } from "./pages/List";
import { AddNew } from "./pages/AddNew";
import { Detail } from "./pages/Detail";
import { Routes, Route } from "react-router-dom";
import {userData, orderData } from "./data";
import { useEffect, useState } from "react";
import { getAllProductsFromApi } from "./callApi";
import { useDispatch, useSelector } from "react-redux";

function App() {
  //lấy dữ liệu tất cả sản phẩm từ api
  const [title, setTitle] = useState("");
  const [limit, setLimit] = useState(0);
  const [page, setPage] = useState(1);
  // redux
  const dispatch = useDispatch();
  const state = useSelector((state) => state.product);

  useEffect(() => {
    //get All products
    getAllProductsFromApi(dispatch, title, limit, page);
  }, [title, page, state.products]);

  useEffect(()=>{
    //get pagination products
    getAllProductsFromApi(dispatch, title, setLimit(6), page);
  },[title , limit , page, state.products])

  return (
    <Routes>
      <Route path="/">
        <Route index element={<Home />} />
        <Route path="login" element={<Login />}></Route>

        <Route path="products">
          <Route
            index
            element={
              <List
                buttonTitle={`Sản Phẩm`}
                searchLabel={`tên sản phẩm`}
                tableTitle={`Tên Sản Phẩm`}
                dataLoad={state.paginationProducts}
                setTitle={setTitle}
                setPage={setPage}
                page={page}
                limit={limit}
              />
            }
          />
          <Route path="new" element={<AddNew />} />
          <Route path=":productId" element={<Detail />} />
        </Route>

        <Route path="users">
          <Route
            index
            element={
              <List
                buttonTitle={`Khách Hàng`}
                searchLabel={`tên khách hàng`}
                tableTitle={`Tên Khách Hàng`}
                dataLoad={userData}
              />
            }
          />
          <Route path="new" element={<AddNew />} />
          <Route path=":userId" element={<Detail />} />
        </Route>

        <Route path="orders">
          <Route
            index
            element={
              <List
                buttonTitle={`Đơn Hàng`}
                searchLabel={`mã đơn hàng`}
                tableTitle={`Mã Đơn Hàng`}
                dataLoad={orderData}
              />
            }
          />
          <Route path="new" element={<AddNew />} />
          <Route path=":orderId" element={<Detail />} />
        </Route>
      </Route>
    </Routes>
  );
}

export default App;
