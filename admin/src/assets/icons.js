import {AiOutlineUser,AiOutlineLock,AiOutlineSearch, AiOutlineDoubleLeft, AiOutlineDoubleRight} from 'react-icons/ai'
import {MdDashboard, MdStorefront} from 'react-icons/md'
import {RiBillFill} from 'react-icons/ri'
import {BsGlobe} from 'react-icons/bs'
import {IoIosArrowBack, IoIosArrowForward} from 'react-icons/io'
const icons = {
    userIcon: <AiOutlineUser/>,
    lockIcon: <AiOutlineLock/>,
    dashIcon: <MdDashboard/>,
    storeIcon: <MdStorefront/>,
    billIcon: <RiBillFill/>,
    viewIcon: <BsGlobe/>,
    searchIcon: <AiOutlineSearch/>,
    doubleArrLeft: <AiOutlineDoubleLeft/>,
    doubleArrRight: <AiOutlineDoubleRight/>,
    arrLeft: <IoIosArrowBack/>,
    arrRight: <IoIosArrowForward/>,
}

export default icons;