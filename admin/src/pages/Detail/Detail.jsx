import { useLocation, useParams } from "react-router-dom";
import ProductDetails from "../../components/Details/ProductDetails/ProductDetails";
import { Navbar } from "../../components/Navbar";
import { Sidebar } from "../../components/Sidebar";
import "./Detail.css";
import { getProductByProductId } from "../../callApi";
import { useDispatch, useSelector } from "react-redux";
import { memo, useEffect } from "react";
const Detail = () => {
  const id = useParams();
  const state = useSelector((state) => state.product);
  const dispatch = useDispatch();

  useEffect(() => {
    getProductByProductId(id.productId, dispatch);
  }, [id.productId, state.products]);

  return (
    <div className="detail">
      <Sidebar />
      <div className="detail__contentWrapper">
        <Navbar />
        <ProductDetails />
      </div>
    </div>
  );
};

export default memo(Detail);
