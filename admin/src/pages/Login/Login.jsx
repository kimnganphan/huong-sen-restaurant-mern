import "./Login.css";
import { Link } from "react-router-dom";
import icons from "../../assets/icons";
const Login = () => {
  const { userIcon, lockIcon } = icons;
  return (
    <div className="login">
      <div className="login__wrapper">
        {/* --------- LOGIN TOP (BACK TOP HOME BUTTON) */}
        <div className="login__wrapperTop">
          <Link to="/">
            <button className="btn-light">Trở về trang chủ</button>
          </Link>
        </div>
        {/* --------- END OF LOGIN TOP (BACK TOP HOME BUTTON) */}

        {/* --------- LOGIN BOTTOM (INPUT) ------------ */}
        <div className="login__wrapperBottom">
          <div className="login__wrapperBottom-title">
            ĐĂNG NHẬP VÀO TÀI KHOẢN
          </div>
          <div className="login__wrapperBottom-inputList">
            <div className="item">
              <label>{userIcon} Tên đăng nhập</label>
              <input type="text" placeholder="Vui lòng nhập tên đăng nhập..." />
            </div>

            <div className="item">
              <label>{lockIcon} Mật khẩu</label>
              <input type="password" placeholder="Vui lòng nhập mật khẩu..." />
            </div>
          </div>

          <div className="login__wrapperBottom-button">
            <button className="btn-light">Đăng Nhập</button>
          </div>
        </div>
        {/* ---------END OF LOGIN BOTTOM (INPUT) */}
      </div>
    </div>
  );
};

export default Login;
