import { Navbar } from "../../components/Navbar";
import { Sidebar } from "../../components/Sidebar";
import AddProducts from "../../components/Add/AddProducts/AddProducts";
import AddUsers from "../../components/Add/AddUsers/AddUsers";
import AddOrders from "../../components/Add/AddOrders/AddOrders";
import "./AddNew.css";

import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
const AddNew = () => {
  const location = useLocation();

  const [component, setComponent] = useState();

  useEffect(() => {
    if (location.pathname === "/products/new") {
      setComponent(<AddProducts />);
    } else if (location.pathname === "/users/new") {
      setComponent(<AddUsers />);
    } else if (location.pathname === "/orders/new") {
      setComponent(<AddOrders />);
    }
  }, [location.pathname]);
  return (
    <div className="addNew">
      <Sidebar />
      <div className="addNew__wrapper">
        <Navbar />
        {component}
      </div>
    </div>
  );
};

export default AddNew;
