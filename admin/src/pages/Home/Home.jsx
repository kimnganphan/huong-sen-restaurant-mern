import { Sidebar } from "../../components/Sidebar";
import { Navbar } from "../../components/Navbar";
import "./Home.css";

const Home = () => {
  return (
    <div className="home-container">
      <Sidebar />
      <div className="home">
        <Navbar />
      </div>
    </div>
  );
};

export default Home;
