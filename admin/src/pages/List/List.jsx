import { Sidebar } from "../../components/Sidebar";
import { Navbar } from "../../components/Navbar";
import { Datatable } from "../../components/Datatable";
import icons from "../../assets/icons";
import "./List.css";
import { Link, useLocation } from "react-router-dom";
import {Pagination} from "../../components/Pagination";


const List = ({ buttonTitle, searchLabel, tableTitle, dataLoad, setTitle, setPage, page, limit }) => {
  const location = useLocation();
  
  //HÀM XỬ LÝ SỰ KIỆN FILTER
  const handleFilterClick = (e) => {
    setPage(1)
    setTitle((e.target.value.toLowerCase()))
  };


  return (
    <div className="list-container">
      <Sidebar />
      <div className="list">
        <Navbar />
        <div className="list__top">
          <div className="list__top-button">
            <Link to={`${location.pathname}/new`}>
              <button>Thêm Mới {buttonTitle}</button>
            </Link>
          </div>

          <div className="list__top-search">
            <label>Tìm kiếm {searchLabel} :</label>
            <div className="search-wrapper">
              <input
                type="text"
                placeholder={`Nhập ${searchLabel}`}
                onChange={(e) => handleFilterClick(e)}
              />
              {icons.searchIcon}
            </div>
          </div>
        </div>

        <Datatable tableTitle={tableTitle} dataLoad={dataLoad} />

        <Pagination dataLoad={dataLoad} setPage={setPage} page={page} limit={limit}/>
      </div>
    </div>
  );
};

export default List;
